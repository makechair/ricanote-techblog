'use strict';

module.exports = {
  url: 'https://ricanote.cloud',
  pathPrefix: '/',
  title: '理系男子の技術ブログ',
  subtitle: 'Web開発を通じて有益と感じた情報を発信していきます。',
  copyright: '© All rights reserved.',
  disqusShortname: '',
  postsPerPage: 4,
  googleAnalyticsId: 'UA-73379983-2',
  useKatex: false,
  menu: [
    {
      label: 'Articles',
      path: '/'
    },
    {
      label: 'About me',
      path: '/pages/about'
    },
    {
      label: 'Contact me',
      path: '/pages/contacts'
    }
  ],
  author: {
    name: '理系男子',
    photo: '/photo.jpg',
    bio: 'Fullstack web developer.',
    contacts: {
      email: '',
      facebook: '#',
      telegram: '#',
      twitter: '#',
      github: '#',
      rss: '',
      vkontakte: '',
      linkedin: '#',
      instagram: '#',
      line: '',
      gitlab: '',
      weibo: '',
      codepen: '',
      youtube: '',
      soundcloud: '',
    }
  }
};
